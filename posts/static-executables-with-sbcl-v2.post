;;;;;
title: Static Executables with SBCL v2
tags: blog, sbcl, common-lisp
date: 2021-02-24 21:50:00
format: md
;;;;;

It's taken me much longer than I hoped, but I finally have a second version of
my patches to build static executables tested and ready to go! This set of
patches vastly improves upon the first by reducing the amount of compilation
needed at the cost of sacrificing a little purity. Additionally I have created
a system that automates the process of building a static executable, along with
other release related tasks.

<!--more-->

## At a Glance

+ The new patch set can be found on
  the
  [static-executable-v2 branch](https://github.com/daewok/sbcl/tree/static-executable-v2) of
  my SBCL fork or at
  `https://www.timmons.dev/static/patches/sbcl/$VERSION/static-executable-support-v2.patch`
  with a detached signature available at
  `https://www.timmons.dev/static/patches/sbcl/$VERSION/static-executable-support-v2.patch.asc`
  signed with GPG
  key [0x9ACF6934](https://www.timmons.dev/static/keys/9ACF6934.pub).
+ You'll definitely want to build SBCL with the `:sb-prelink-linkage-table`
  feature (newly added by the patch). You'll probably also want the
  `:sb-linkable-runtime` feature (already exists, but the patch also enables it
  on arm/arm64).
+ The new patch lets you build a static executable with less compilation of
  Lisp code.
+ The [asdf-release-ops system](https://gitlab.com/daewok/asdf-release-ops)
  automates the process of building a static executable by tying it into ASDF.

## What's New?

If you need a refresher about what static executables are or what use cases
they're good for, see
my [previous post on this topic](static-executables-with-sbcl.html).

With my previous patch, the only way you could create a static executable was
to perform the following steps:

1. Determine the foreign symbols needed by your code. The easiest way to do
   this is to compile all your Lisp code and then dump the information from the
   image.
2. From that list of foreign symbols, create a C file that contains fills an
   array with references to those symbols.
3. Recompile the SBCL core and runtime with this new file, additionally
   disabling libdl support and linking against your foreign libraries.
4. (Re)compile all your Lisp code with the new runtime (if you made an image in
   step 1 it will not be compatible with the new runtime due to feature and
   build ID mismatches).
5. Dump the executable.

In the most general case, this involved compiling your entire Lisp image
twice. After some #lisp discussions, I realized there was a better way of doing
this. While the previous process still works, the new recommended process now
looks like:

1. Build the image you would like to make into a static executable and save it.
2. Dump the foreign symbol info from this image and write the C file that SBCL
   can use to prelink itself.
3. Compile that C file and link it into an existing sbcl.o file to make a new
   runtime. sbcl.o is the SBCL runtime in object form, created when building
   with the `:sb-linkable-runtime` feature.
4. Load the image from step 1 into your new runtime. It will be compatible
   because the build ID and feature set are the same!
5. Dump your now static executable.

This new process can significantly reduce the amount of time needed to make an
executable. Plus it lets you take more advantage of image based
development. It's fairly trivial to build an image exactly like you want, dump
it, and then pair it with a custom static runtime to make a static executable.

There were two primary challenges that needed to be overcome for this version
of the patch set.

First, the SBCL core had to be made robust to every libdl function
uncondtionally returning an error. Since we want the feature set to remain
constant we can't recompile the runtime with `#-os-provides-dlopen`. Instead,
we take advantage of the fact that Musl libc lets you link static executables
against libdl, but all those functions are noops. This is the "purity"
sacrifice I alluded to above.

Second, since we are reusing a image, the prelink info table (the generated C
file) needed to order the symbols exactly as the image expects them to be
ordered. The tricky bit here is that some libraries (like `cl-plus-ssl`) add
symbols to the linkage table that will always be undefined. `cl-plus-ssl` does
this in order to support a wide range of openssl versions. The previous patch
set unconditionally filtered out undefined symbols, which horribly broke things
in the new approach.

## More Documentation

As before, after applying the patch you'll find a README.static-executable file
in the root of the repo. You'll also find a Dockerfile and an example of how to
use it in the README.static-executable.

You can also check out the tests and documentation in
the [asdf-release-ops system](https://gitlab.com/daewok/asdf-release-ops).

## Known Issues

- The `:sb-prelink-linkage-table` feature does not work on 32-bit ARM + Musl
  libc >= 1.2. Musl switched to 64-bit time under the hood while still
  mataining compatibility with everything compiled for 32-bit time.
   
  The issue is how they maintained backwards compatibility. Every time related
  symbol still exists and implements everything on top of the 32-bit time
  interface. However, if you include the standard header file where the symbol
  is defined *or* you look up the symbol via `dlsym` you actually get a pointer
  to the 64-bit time version of the symbol. We can't use `dlsym` (it doesn't
  work in static executables). And the generated C file doesn't include any
  headers.
   
  This *could* be fixed if someone is motiviated enough to create/find a
  complete, easy to use map between libc symbols and the headers that define
  them and integrate it into the prelink info generator.

- The `:sb-prelink-linkage-table` works on Windows but causes test
  failures. The root issue is that mingw64 has implemented their own
  libm. Their trig functions are fast, but use inaccurate instructions (like
  FSIN) under the hood. When prelinking these inaccurate implementations are
  used instead of the more accurate ones (from msvcrt.dll ?) found when using
  `dlsym` to look up the symbol.

## Next Steps

1. I would love to get feedback on this approach and any ideas on how to
   improve it! Please drop me a line (etimmons on Freenode or daewok on
   Github/Gitlab) if you have suggestions.

2. I've already incorporated static executables
   into [CLPM](https://www.clpm.dev) and will be distributing them starting
   with v0.4.0! I'm going to continue rolling out static executables in my
   other projects.

3. Pieces of the patch set are now solid enough that I think they can be
   submitted for upstream consideration. I'll start sending them after the
   current 2.1.2 freeze.
